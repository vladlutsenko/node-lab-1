const fs = require('fs');
const readline = require('readline');
const path = require('path');


var readGas = new Promise((resolve, reject) => {
    const rlGas = readline.createInterface({
        input: fs.createReadStream(path.join(__dirname, './audit/gas.csv'))
    })
    .on('line', (line)=>{
        var row = line.split(',');
        dataGas.push(row);
    })
    .on('close', () => {
        resolve(dataGas);
    });

})


var readWater = new Promise((resolve, reject) => {
    const rlWater = readline.createInterface({
        input: fs.createReadStream(path.join(__dirname, './audit/water.csv'))
    })
    .on('line', (line)=>{
        var row = line.split(',');
        dataWater.push(row);
    })
    .on('close', () => {
        resolve(dataWater);
    });
})


var dataGas =[];
var dataWater =[];


function analyse(data, type) {
    var processedData =[];
    data.forEach(d => {
        record ={};
        record.date=d[0];
        record.value=d[1];
        record.type=type;
        userExists=false;
        processedData.forEach(pd => {
            if (pd.name==d[2]) {
                pd.data.push(record);
                userExists=true;
            }
        });
        if (!userExists) {
            user={};
            user.name=d[2];
            user.data=[];
            user.data.push(record);
            processedData.push(user);
        }
    });
    return processedData;
}


function mergeData(dataGas, dataWater) {
    var processedData =[];
    dataGas.forEach(dg => {
        userExists=false;
        processedData.forEach(pd => {
            if (pd.name==dg.name) {
                dg.data.forEach(record => {
                    pd.data.push(record);
                    pd.count++;
                });
                userExists=true;
            }
        });
        if (!userExists) {
            user={};
            user.name=dg.name;
            user.data=[];
            user.count=0;
            dg.data.forEach(record => {
                user.data.push(record);
                user.count++;
            });
            processedData.push(user);
        }
    });
    dataWater.forEach(dw => {
        userExists=false;
        processedData.forEach(pd => {
            if (pd.name==dw.name) {
                dw.data.forEach(record => {
                    pd.data.push(record);
                    pd.count++;
                });
                userExists=true;
            }
        });
        if (!userExists) {
            user={};
            user.name=dw.name;
            user.data=[];
            user.count=0;
            dw.data.forEach(record => {
                user.data.push(record);
                user.count++;
            });
            processedData.push(user);
        }
    });
    return processedData;
}


function printData(data) {
    data.forEach(user => {
        console.log(
            `User ${user.name} внес ${user.count} раз данные \n` 
        );
        user.data.forEach(record => {
            console.log(
                `- ${record.date}: type: ${record.type} value: ${record.value}`
            );
        });
        console.log('\n\n');
    });
}


Promise.all([readGas, readWater]).then(() => {              //ждём чтения обоих файлов
    analysedGas=analyse(dataGas, 'gas');                    //данные из файла gas разбираем по пользователям
    analysedWater=analyse(dataWater, 'water');              //данные из файла water разбираем по пользователям
    var mergedData = mergeData(analysedGas, analysedWater); //объеденяем данные из двух файлов в один массив
    printData(mergedData);                                  //выводим данные
});