const fs = require('fs');
const readline = require('readline');
const path = require('path');


const rl = readline.createInterface({
    output: process.stdout,
    input: process.stdin
});


var data = {
    type: '',
    value: '',
    date: '',
    name: ''
}


async function asker() {
    data.type = await Ask("type ");
    data.value = await Ask("value ");
    data.date = await Ask("date ");
    data.name = await Ask("name ");
    
    rl.close();
    validatedData=await validate(data);
    writeToFile(JSON.stringify(validatedData));
}


function Ask(q) {
    return new Promise((resolve, reject) => {
        rl.question(q, (data)=>{
            if (data) {
                console.log(data, 'received');
                resolve(data);
            }
            else{
                reject(new Error('something went wrong'));
            }
       });

    });
    
}


function writeToFile(data) {
    date = Date.now();
    fs.writeFileSync(path.join(__dirname, `./dist/${date}-info.json`), data);
}


function validate(data) {
    return new Promise((resolve, reject) => {
        if (data.type!='water' && data.type!='gas') {
            reject('data type incorrect');
            return;
        }
        if (data.value.length!=6 || isNaN(parseInt(data.value))) {
            reject('data value must be 6 digits');
            return;
        }
        var dateReg = /^\d{2}-\d{2}-\d{4}$/;
        if (!dateReg.test(data.date)) {
            reject('wrong date format');
            return;
        }
        resolve(data);
    })
}


asker();